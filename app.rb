require 'socket'
require './fn/randomName'

server = TCPServer.new 3000

while session = server.accept
    request = session.gets
    puts request
    
    session.print "HTTP/1.1 200\r\n"
    session.print "Content-Type: text/html\r\n"
    session.print "Env: Ruby #{ENV['RUBY_VERSION']}\r\n"
    session.print "\r\n"
    
    if request.split(' ')[1] == "/version"
        session.print "#{ENV['COMMIT']}"
    else
        session.print getRandomName()
    end

    session.close
  end
