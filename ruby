#!/bin/sh
docker run --rm --init -it \
  --entrypoint ruby \
  --user $(id -u) \
  --workdir /src \
  -p 3000:3000 \
  -v $(pwd):/src \
  -e COMMIT=$(git rev-parse --short=8 HEAD) \
  ruby:3.1-slim "$@"
 